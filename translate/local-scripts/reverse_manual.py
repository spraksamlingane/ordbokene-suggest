import requests
existing_lemmas = {x[0] for x in requests.get("https://ord.uib.no/bm/fil/lemma.json").json()}
with open("translate/manual/nn_til_bm2.txt", "w", encoding="utf-8") as ut:
    with open("translate/manual/bm_til_nn.txt", encoding="utf-8") as inn:
        for line in inn.readlines():
            words = line.strip().split("|")
            for w in words[1:]:
                if w not in existing_lemmas:
                    ut.write(w + "|" + words[0]+"\n")

#print(existing_lemmas)