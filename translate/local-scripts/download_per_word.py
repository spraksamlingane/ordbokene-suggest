"""
Translates only lemmas. Uses the apertium handler perWord handler if the lemma only contains letters

"""
import requests
import re
import json

articles = {
    "bm": [x for x in requests.get("https://ord.uib.no/bm/fil/lemma.json").json() if x[0][0] != "-" and x[0][-1] != "-"],
    "nn": [x for x in requests.get("https://ord.uib.no/nn/fil/lemma.json").json() if x[0][0] != "-" and x[0][-1] != "-"],
}

searches = {
    "bm": {},
    "nn": {},
}
searches_pos = {
    "bm": {},
    "nn": {}
}



for d in ["bm", "nn"]:
    for lemma, ID, tag in articles[d]:
        tag = tag.split("_")[0]
        searches[d][lemma] = searches[d].get(lemma, set()) | {ID}

        
        searches_pos[d][lemma] = searches_pos[d].get(lemma, {})
        searches_pos[d][lemma][tag] = searches_pos[d][lemma].get(tag, set()) | {ID}


def per_word(text, frm):
    pair = {"nn": "nno-nob", "bm": "nob-nno"}[frm]
    
    try:
        return requests.get(f"http://localhost:2737/perWord?lang={pair}&modes=biltrans&q={text}").json()
    except Exception as e:
        print(e)


def skip_tag(text):
    # Compound words omitted because their components are tagged separately
    if "*" in text or "<cmp>" in text:
        return True
    
    if "<n>"in text and not ("<sg>" in text and "<ind>" in text):
        return True
    
    if "<vblex>" in text and "<inf>" not in text:
        return True
    
    if "<np>" in text or "<acr>" in text or "<pp>" in text:
        return True
    
    if "<adj>" in text and ("<comp>" in text or "<sup>" in text):
        return True
    

def run_translations(d, translation_map = {}):
    other = {"bm" : "nn", "nn": "bm"}[d]
    count = 0
    iteration = 0
    for lemma, value in searches_pos[d].items():
        iteration += 1
        if lemma in translation_map:
            continue
            
        tags = list(value.keys())
        
        if lemma in searches[other]:
            one_result_in_both = len(searches[d][lemma]) == 1 and len(searches[other][lemma]) == 1 
            if one_result_in_both and list(tags)[0] == list(searches_pos[other][lemma].keys())[0]:
                continue
                
        filtered_tags = {x for x in tags if tag not in {'PROPN', 'SYM'} and not (tag == "ABBR" and "." not in lemma)}
        if not filtered_tags:
            continue
            
        if lemma.isupper() or len(lemma) == 1:
            continue
            
        if lemma.islower() and lemma.isalpha():
            response = per_word(lemma, d)
            for item in response[:-1]:
                for biltrans in item["biltrans"]:
                    if not skip_tag(biltrans):
                        translation_map[lemma] = translation_map.get(lemma, set())| {biltrans}
        count += 1

        if count % 200 == 0:
            print(count)
            print((iteration/len(searches[d]))*100, "%")   



translation_map = {}
run_translations("bm", translation_map)

with open("translate/auto/bm_per_word.json", "w") as ut:
    json.dump({a: list(b) for a, b in translation_map.items()}, ut)


translation_map = {}
run_translations("nn", translation_map)

with open("translate/auto/nn_per_word.json", "w") as ut:
    json.dump({a: list(b) for a, b in translation_map.items()}, ut)