import requests
import json
import re

def translate(word, to, base="http://localhost:2737"):
    if to == "nn":
        pair = "nob|nno"
    if to == "bm":
        pair = "nno|nob"
    try:
        return requests.get(f"{base}/translate?langpair={pair}&q={word}").json()
    except:
        return {}

lemmas = {
    "bm": {x[0] for x in requests.get("https://ord.uib.no/bm/fil/lemma.json").json()},
    "nn": {x[0] for x in requests.get("https://ord.uib.no/nn/fil/lemma.json").json()},
}


def translate_all(d, to):
    translations = {}
    teller = 0
    for lemma in lemmas[d]:
        teller += 1
        if teller % 100 == 0:
            print(teller)
        response = translate(lemma, to)
        if response["responseStatus"] != 200:
            print("ERROR", response)
            continue
        translations[lemma] = response["responseData"]["translatedText"]
    return translations


bm2nn = translate_all("bm", "nn")

with open("basic_bm2nn2.json", "w") as out:
    filtered = {key: value for key, value in bm2nn if key != value}
    json.dump(filtered, out, indent=4, sort_keys=True, ensure_ascii=False)

nn2bm = translate_all("nn", "bm")

with open("basic_nn2bm.json", "w") as out:
    filtered = {key: value for key, value in nn2bm if key != value}
    json.dump(filtered, out, indent=4, sort_keys=True, ensure_ascii=False)