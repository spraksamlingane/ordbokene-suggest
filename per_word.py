from tools.lookup import word_exists, tag_overlap, result_subset, TAG_MAPS
import requests
import json
import re

"""
Filter tagged translations from apertium perWord
https://wiki.apertium.org/wiki/Apertium-apy#:~:text=URL,/perWord

Translations are stored in the aperium stream format:
https://wiki.apertium.org/wiki/Apertium_stream_format

"""

class PerWord:
    def __init__(self, origin):
        self.origin = origin
        self.target = {"bm": "nn", "nn": "bm"}[origin]
        self.lemmas = [(x[0], x[2].split("_")[0]) for x in requests.get(f"https://ord.uib.no/{origin}/fil/lemma.json").json()]
        self.translations = self.load_translations()
    

    def load_translations(self):
        with open(f"translate/auto/{self.origin}_per_word.json") as infile:
            return json.load(infile)
    
    def detect_tag(self, text):
        ''' Detect POS tags for lemmas in the apertium stream format: https://wiki.apertium.org/wiki/List_of_symbols '''
        
        if "<n>" in text and "<sg>" in text and "<ind>" in text:
            return "NOUN"
        
        if "<np>" in text:
            return "PROPN"

        if "<vblex>" in text:
            return "VERB"

        if "<adj>" in text:
            return "ADJ"

        if "<adv>" in text:
            # TODO: omit participle
            return "ADV"
        
        if "<prn>" in text:
            return "PRON"

        if "<det>" in text:
            return "DET"
        
        if "<pr>" in text:
            return "ADP"
        
        if "<ij>" in text:
            return "INTJ"
        
        if "<cnjcoo>" in text:
            return "CCONJ"
        
        if "<cnjsub>" in text:
            return "SCONJ"   

    def clean(self, translation):
        return re.sub(r'<\w*\>|[¹²#]', '', translation)

    def filter(self):
        filtered = {}
        for lemma, wc in self.lemmas:

            if not word_exists(lemma, self.origin):
                continue

            for translation in self.translations.get(lemma, []):
                if wc == self.detect_tag(translation):
                    translated_lemmas = self.clean(translation).split("/")
                    for translated_lemma in translated_lemmas:
                        if translated_lemma == lemma or translated_lemma == "":
                            continue

                        filtered[lemma] = list(set(filtered.get(lemma, [])) | {translated_lemma})
        return filtered
                


            





if __name__== "__main__":
    
    pw_nn = PerWord('bm')
    with open(".tmp/bm_per_word.json", "w", encoding="utf8") as ut:
        json.dump({key: value for key, value in pw_nn.filter().items() if len(value) > 1}, ut, ensure_ascii=False)