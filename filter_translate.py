import json
from tools.lookup import word_exists, tag_overlap, result_subset, TAG_MAPS
from per_word import PerWord
import sys


def validate_manual(data, source, target):
    missing_words = []
    approved = {}

    report_missing_word = open(f".report/{source}2{target}_MANUAL_missing_lemma.txt", "w", encoding="utf-8")
    report_missing_word.write("Oversikt over oversatte lemmaer som ikke finnes i ordboken.\n")
    report_missing_translation = open(f".report/{source}2{target}_MANUAL_missing_translation.txt", "w", encoding="utf-8")
    report_missing_translation.write("Oversikt over maskinoversettelser som ikke finnes i ordboken.\n")
    report_subset_result = open(f".report/{source}2{target}_MANUAL_subset_result.txt", "w", encoding="utf-8")
    report_subset_result.write("Oversikt over forslag som gir resultater som allerede vises i resultatlisten\n")
    report_no_tag_overlap = open(f".report/{source}2{target}_MANUAL_no_tag_overlap.txt", "w", encoding="utf-8")
    report_no_tag_overlap.write("Oversikt over forslag som ikke har en eller flere ordklasser til felles med ordet det er oversatt fra\n")

    for word, translations in data.items():
        printed = False
        if not word_exists(word, source):
            if not printed:
                report_missing_translation.write(source + "|" + translation + "\n")
                printed = True
            report_missing_translation.write(source + ": " + word + "\n")
            missing_words.append(source + "/" + word)



        for translation in translations:
            if not word_exists(translation, target):
                if not printed:
                    report_missing_translation.write(source + "|" + translation + "\n")
                    printed = True
                report_missing_translation.write(target + ": " + translation + "\n")
                missing_words.append(target + "/" + translation)
        
            if not printed and tag_overlap(word, translation, source, target):
                if not printed and not result_subset(translation, word, target):
                    approved[word] = approved.get(word, []) + [translation]
                else:
                    report_subset_result.write(word + "|" + translation + "\n")
            
            else:
                report_no_tag_overlap.write(word + "|" + translation + "\n")


    if missing_words:
        sys.exit("Missing words:"+ ", ".join(missing_words))

    return approved


def advaced_translate(source, target, overridden):
    # Include subset check since there is more than one translation

    pw_translations = PerWord(source).filter()
    approved = {}

    for word, translations in pw_translations.items():
        if word in overridden:
            continue
        for translation in translations:
            if not word_exists(translation, target) or "*" in translation:
                continue

            if not tag_overlap(word, translation, source, target):
                continue

            if not result_subset(translation, word, target):
                approved[word] = approved.get(word, []) + [translation]


    return approved


def basic_translate(source, target, overridden):
    with open(f"translate/auto/basic_{source}2{target}.json") as inn:
        translation_dict = json.load(inn)

    approved = {}

    report_missing_word = open(f".report/{source}2{target}_AUTO_missing_lemma.txt", "w", encoding="utf-8")
    report_missing_word.write("Oversikt over oversatte lemmaer som ikke finnes i ordboken.\n")
    report_missing_translation = open(f".report/{source}2{target}_AUTO_missing_translation.txt", "w", encoding="utf-8")
    report_missing_translation.write("Oversikt over maskinoversettelser som ikke finnes i ordboken.\n")
    report_no_tag_overlap = open(f".report/{source}2{target}_AUTO_no_tag_overlap.txt", "w", encoding="utf-8")
    report_no_tag_overlap.write("Oversikt over forslag som ikke har en eller flere ordklasser til felles med ordet det er oversatt fra\n")
    

    for word, translation in translation_dict.items():
        # Ignore translations that are identical to the word , haven't been successfully translated by apertium
        # or is overriden by the manual list
        if word in overridden or word == translation:
            continue

        if not word_exists(word, source):
            report_missing_translation.write(word + "|" + translation + "\n")
            continue

        if not word_exists(translation, target) or "*" in translation:
            report_missing_translation.write(word + "|" + translation + "\n")
            continue
        
        if not tag_overlap(word, translation, source, target):
            report_no_tag_overlap.write(word + "|" + translation + "\n")
            continue

        approved[word] = translation



    report_missing_word.close()
    report_missing_translation.close()
    #report_subset_result.close()
    report_no_tag_overlap.close()
    return approved

    




if __name__== "__main__":
    
    if len(sys.argv) > 1:
        TYPE, SOURCE, TARGET = sys.argv[1:]
        
    else:
        SOURCE = "bm"
        TARGET = "nn"
        TYPE = "auto"
    
    with open(f"translate/manual/{SOURCE}_til_{TARGET}.txt", encoding="utf-8") as inn:
        manual = {x.split("|")[0].strip(): x.split("|")[1:] for x in inn}
        manual = {key: [x.strip() for x in value] for key, value in manual.items()}

    
    if TYPE == "auto":
        translations = basic_translate(SOURCE, TARGET, overridden = set(manual.keys()))
        translations = {key: [value] for key, value in translations.items()}
        perword_translations = advaced_translate(SOURCE, TARGET, overridden= set(manual.keys()))
        translations.update(perword_translations)

        with open(f".tmp/{SOURCE}2{TARGET}_auto.json", "w", encoding="utf-8") as out:
            json.dump(translations, out, ensure_ascii=False)


    else:
        validated_manual = validate_manual(manual, SOURCE, TARGET)   
        with open(f".tmp/{SOURCE}2{TARGET}_manual.json", "w", encoding="utf-8") as out:
            json.dump(validated_manual, out, ensure_ascii=False)

