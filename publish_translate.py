import sys
import json
import requests
from collections import defaultdict

BASE_URL = "https://ord.uib.no/{}/fil/lemma.json"

def load_json_from_url(dict_name):
    url = BASE_URL.format(dict_name)
    response = requests.get(url)
    return response.json()

def group_lemmas_by_article_id(data):
    grouped = defaultdict(set)
    for lemma_data in data:
        lemma, article_id = lemma_data[0], lemma_data[1]
        grouped[article_id].add(lemma)  
    return grouped

def get_lemmas_by_article(article_data):
    article_dict = defaultdict(list)
    for lemma_data in article_data:
        lemma, article_id = lemma_data[0], lemma_data[1]
        article_dict[article_id].append(lemma)
    return article_dict

if __name__ == "__main__":
    SOURCE_DICT = sys.argv[1] if len(sys.argv) > 1 else "bm"
    TARGET_DICT = sys.argv[2] if len(sys.argv) > 1 else "nn"
    print("SOURCE_DICT", SOURCE_DICT)
    print("TARGET_DICT", TARGET_DICT)


    source_data = load_json_from_url(SOURCE_DICT)
    target_data = load_json_from_url(TARGET_DICT)

    target_grouped_by_article = group_lemmas_by_article_id(target_data)

    # Load automatic translations
    with open(f".tmp/{SOURCE_DICT}2{TARGET_DICT}_auto.json", encoding="utf-8") as inp:
        translations = json.load(inp)

    # Overwrite with the manual list
    with open(f".tmp/{SOURCE_DICT}2{TARGET_DICT}_manual.json", encoding="utf-8") as inp:
        for key, value in json.load(inp).items():
            translations[key] = value

    final_output = []

    for source_lemma, target_lemmas in translations.items():
        processed_target_groups = []

        for target_lemma in target_lemmas:
            combined_target_lemmas = set([target_lemma])  

            for article_id, lemmas_in_article in target_grouped_by_article.items():
                if target_lemma in lemmas_in_article:
                    combined_target_lemmas.update(lemmas_in_article)

            combined_target_lemmas_sorted = sorted(combined_target_lemmas)
            processed_target_groups.append("/".join(combined_target_lemmas_sorted))

        final_output.append(f"{source_lemma}|{'|'.join(processed_target_groups)}")

    with open(f"{SOURCE_DICT}2{TARGET_DICT}.txt", "w", encoding="utf-8") as out:
        for line in final_output:
            out.write(line + "\n")