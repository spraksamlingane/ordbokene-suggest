import requests
LEMMAS =  {
        "bm": requests.get("https://ord.uib.no/bm/fil/lemma.json").json(),
        "nn": requests.get("https://ord.uib.no/nn/fil/lemma.json").json()
    }


def prepare_lookup_dicts():
    id_maps = {"bm": {}, "nn": {}}
    tag_maps = {"bm": {}, "nn": {}}
    for d in ["bm", "nn"]:
        for lemma, ID, tag, _ in LEMMAS[d]:
            id_maps[d][lemma] = id_maps[d].get(lemma, set()) | {ID}
            tag_maps[d][lemma] = tag_maps[d].get(lemma, set()) | {tag.split("_")[0]}

    return id_maps, tag_maps


ID_MAPS, TAG_MAPS = prepare_lookup_dicts()


def word_exists(word, d):
    return ID_MAPS[d].get(word)


def tag_overlap(word, translation, source, target):
    first_word_tags = TAG_MAPS[source].get(word)
    for tag in TAG_MAPS[target].get(translation):
        if tag == "EXPR" or tag in first_word_tags or "EXPR" in first_word_tags:
            return True

def result_subset(translation, word, d):
    return ID_MAPS[d][translation].issubset(ID_MAPS[d].get(word, set()))
    